import React, { FC } from 'react';
import AuthLayout from './AuthLayout';

export const AdminPage: FC = () => {
  return (
    <AuthLayout>
      <div>This is AdminPage</div>
    </AuthLayout>
  );
};

export default AdminPage;
